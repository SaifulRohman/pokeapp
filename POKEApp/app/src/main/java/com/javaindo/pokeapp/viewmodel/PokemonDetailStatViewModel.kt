package com.javaindo.pokeapp.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.javaindo.pokeapp.local.PokemonDatabase
import com.javaindo.pokeapp.model.pokemon_monster.PokemonStatModel
import com.javaindo.pokeapp.repositories.PokemonDetailRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PokemonDetailStatViewModel (val pokeDetailRepo : PokemonDetailRepository,val context : Context): ViewModel(){

    private var idPoke : Int = 0
    var pokemonStat : MutableLiveData<PokemonStatModel> = MutableLiveData<PokemonStatModel>()

    suspend fun processData(){
        pokeDetailRepo.processDataPokemon(idPoke)
    }

    suspend fun processGetDataStat(){
        GlobalScope.launch {
            val db = PokemonDatabase(context)
            val pokeStat = db.PokeStatsDao().getPokeStats(idPoke)

            if(pokeStat != null){
                pokeStat.let {
                    val pokeStatMdl = PokemonStatModel()
                    pokeStatMdl.hp = it.hp
                    pokeStatMdl.abilities = it.abilities
                    pokeStatMdl.atk = it.atk
                    pokeStatMdl.def = it.def
                    pokeStatMdl.double_dmg_from = it.double_dmg_from
                    pokeStatMdl.id = it.id
                    pokeStatMdl.satk = it.satk
                    pokeStatMdl.sdef = it.sdef
                    pokeStatMdl.spd = it.spd
                    pokeStatMdl.species_name = it.species_name
                    pokeStatMdl.species_note = it.species_note
                    pokeStatMdl.sprites_normal = it.sprites_normal
                    pokeStatMdl.sprites_shiny = it.sprites_shiny
                    pokeStatMdl.types = it.types

                    pokemonStat.postValue(pokeStatMdl)
                }
            }
        }
    }

    fun setId(id : Int){
        this.idPoke = id
    }

}