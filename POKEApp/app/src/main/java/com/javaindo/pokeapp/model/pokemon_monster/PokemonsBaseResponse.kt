package com.javaindo.pokeapp.model.pokemon_monster


class PokemonsBaseResponse(
    val count: Int,
    val next: String,
    val previous: String,
    val results: List<results>
)