package com.javaindo.pokeapp.model.pokemon_monster


data class TypeResponse(
    val slot: Int,
    val typeResponse: TypeXResponse
)