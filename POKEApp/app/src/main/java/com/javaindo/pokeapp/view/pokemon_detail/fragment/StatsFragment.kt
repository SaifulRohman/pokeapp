package com.javaindo.pokeapp.view.pokemon_detail.fragment

import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.javaindo.pokeapp.R
import com.javaindo.pokeapp.databinding.FragmentStatsMonsterBinding
import com.javaindo.pokeapp.repositories.PokemonDetailRepository
import com.javaindo.pokeapp.utilities.StringManagement
import com.javaindo.pokeapp.viewmodel.PokemonDetailStatViewModel
import kotlinx.coroutines.launch

class StatsFragment : Fragment(){

    private var _binding : FragmentStatsMonsterBinding? = null
    private val binding get() = _binding!!
    lateinit var viewModel :PokemonDetailStatViewModel

    private var tabTitle = ""
    private var id = "0"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentStatsMonsterBinding.inflate(inflater,container,false)
        val view = binding.root
        return view
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tabTitle = arguments?.getString("stats").toString()
        id = arguments?.getString("idPokemon").toString()

        activity?.let {
                viewModel = ViewModelProvider(it, object : ViewModelProvider.Factory {
                override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                    if (modelClass.isAssignableFrom(PokemonDetailStatViewModel::class.java)) {
                        return PokemonDetailStatViewModel(pokeDetailRepo = PokemonDetailRepository(context!!), context!!) as T
                    } else throw IllegalArgumentException("Unknown ViewModel class")
                }
            }).get(PokemonDetailStatViewModel::class.java)

            viewModel.setId(id.toInt())
            lifecycleScope.launch {
                viewModel.processData()

                viewModel.processGetDataStat()
            }

            viewModel.pokemonStat.observe(it, Observer {
                binding.incldStats.txvProgressHP.setBackgroundResource(R.color.progressbarGray)
                binding.incldStats.txvProgressHP.max = 100
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                    binding.incldStats.txvProgressHP.progressDrawable.colorFilter = BlendModeColorFilter(Color.YELLOW,BlendMode.SRC_IN)
//                } else{
//
//                    binding.incldStats.txvProgressHP.progressDrawable.setColorFilter(Color.YELLOW,PorterDuff.Mode.SRC_IN)
//                }
                binding.incldStats.txvProgressHP.setProgress(it.hp)
                binding.incldStats.txvNumberHP.setText(StringManagement.zeroAdd(it.hp.toString()))

                binding.incldStats.txvProgressATK.setBackgroundResource(R.color.progressbarGray)
                binding.incldStats.txvProgressATK.max = 100
                binding.incldStats.txvProgressATK.setProgress(it.atk)
                binding.incldStats.txvNumberATK.setText(StringManagement.zeroAdd(it.atk.toString()))

                binding.incldStats.txvProgressDEF.setBackgroundResource(R.color.progressbarGray)
                binding.incldStats.txvProgressDEF.max = 100
                binding.incldStats.txvProgressDEF.setProgress(it.def)
                binding.incldStats.txvNumberDEF.setText(StringManagement.zeroAdd(it.def.toString()))

                binding.incldStats.txvProgressSATK.setBackgroundResource(R.color.progressbarGray)
                binding.incldStats.txvProgressSATK.max = 100
                binding.incldStats.txvProgressSATK.setProgress(it.satk)
                binding.incldStats.txvNumberSATK.setText(StringManagement.zeroAdd(it.satk.toString()))

                binding.incldStats.txvProgressSDEF.setBackgroundResource(R.color.progressbarGray)
                binding.incldStats.txvProgressSDEF.max = 100
                binding.incldStats.txvProgressSDEF.setProgress(it.sdef)
                binding.incldStats.txvNumberSDEF.setText(StringManagement.zeroAdd(it.sdef.toString()))

                binding.incldStats.txvProgressSPD.setBackgroundResource(R.color.progressbarGray)
                binding.incldStats.txvProgressSPD.max = 100
                binding.incldStats.txvProgressSPD.setProgress(it.spd)
                binding.incldStats.txvNumberSPD.setText(StringManagement.zeroAdd(it.spd.toString()))



            })
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}