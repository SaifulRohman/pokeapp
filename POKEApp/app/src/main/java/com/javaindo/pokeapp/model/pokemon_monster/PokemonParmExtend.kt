package com.javaindo.pokeapp.model.pokemon_monster

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

class PokemonParmExtend {

    var id: Int = 0
    var hp: Int = 0
    var atk: Int = 0
    var def: Int = 0
    var satk: Int = 0
    var sdef: Int = 0
    var spd: Int = 0
    var double_dmg_from: String = ""
    var abilities: String = ""
    var sprites_normal: String = ""
    var sprites_shiny: String = ""
    var species_name: String = ""
    var species_note: String = ""
    var types : String = ""

}