package com.javaindo.pokeapp.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.javaindo.pokeapp.local.dao.PokeMonstDao
import com.javaindo.pokeapp.local.dao.PokeStatsDao
import com.javaindo.pokeapp.local.model.PokeMonstEntity
import com.javaindo.pokeapp.local.model.PokemonStatEntity

@Database(entities = [PokeMonstEntity::class,
                     PokemonStatEntity::class], version = 1)
abstract class PokemonDatabase : RoomDatabase(){

    companion object{
        @Volatile private var instance : PokemonDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context,
            PokemonDatabase::class.java, "pokemon.db")
            .build()
    }

    abstract fun PokeMonstDao() : PokeMonstDao
    abstract fun PokeStatsDao() : PokeStatsDao
}