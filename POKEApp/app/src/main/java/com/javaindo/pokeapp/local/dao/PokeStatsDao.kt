package com.javaindo.pokeapp.local.dao

import androidx.paging.PagingSource
import androidx.room.*
import com.javaindo.pokeapp.local.model.PokeMonstEntity
import com.javaindo.pokeapp.local.model.PokemonStatEntity

@Dao
interface PokeStatsDao {

    @Query("SELECT * FROM pokemon_stat where index_poke_stat BETWEEN :offset AND :offsetMax")
    suspend fun getAllPokeStatsSuspend(offset: Int, offsetMax: Int):List<PokemonStatEntity>

    @Query("SELECT * FROM pokemon_stat where id == :idPoke")
    suspend fun getPokeStats(idPoke : Int):PokemonStatEntity

    @Insert
    fun insertPokeStats(vararg pokemon_stat: PokemonStatEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPokeStatsAll(pokeStatsEntityList : ArrayList<PokemonStatEntity>)

    @Delete
    fun deletePokeStats(pokeStatsEntity: PokemonStatEntity)

    @Update
    fun udpatePokeStats(vararg pokeStatsEntity: PokemonStatEntity)
}