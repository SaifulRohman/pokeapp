package com.javaindo.pokeapp.view.pokemon_detail.adapter

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.javaindo.pokeapp.view.pokemon_detail.PokemonDetailActivity
import com.javaindo.pokeapp.view.pokemon_detail.fragment.EvolutionFragment
import com.javaindo.pokeapp.view.pokemon_detail.fragment.StatsFragment


private const val STAT_TAB = "STATS"
private const val EVOLUTION_TAB = "EVOLUTION"

class PokeMonstPagerAdapter(fm : PokemonDetailActivity,val idPokemon : String) : FragmentStateAdapter(fm) {

    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return when (position){
            0 -> {
                val fragment = StatsFragment()
                fragment.arguments = Bundle().apply {
                    putString("stats",STAT_TAB)
                    putString("idPokemon",idPokemon)
                }
                return fragment
            }
            else -> {
                val fragment = EvolutionFragment()
                fragment.arguments = Bundle().apply {
                    putString("evolution", EVOLUTION_TAB)
                    putString("idPokemon",idPokemon)
                }
                return fragment
            }
        }
    }

}