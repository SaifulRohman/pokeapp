package com.javaindo.pokeapp.utilities

enum class Status {
    RUNNING,
    SUCCESS,
    FAILED
}