package com.javaindo.pokeapp.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import com.javaindo.pokeapp.repositories.PokemonMonsterRepository
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.javaindo.pokeapp.view.pokemon_monster.paging.PokeMonsterPagingSource
import kotlinx.coroutines.launch

class PokemonMonsterViewModel (private val pokeMonsterRepo : PokemonMonsterRepository,context: Context) : ViewModel() {

    val pokeFlow = Pager(PagingConfig(20)) {
        PokeMonsterPagingSource(context,pokeMonsterRepo)
    }.flow.cachedIn(viewModelScope)

//    suspend fun processData(){
//        pokeMonsterRepo.processData()
//    }

}