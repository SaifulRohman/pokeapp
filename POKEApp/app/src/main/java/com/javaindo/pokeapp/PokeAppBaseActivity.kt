package com.javaindo.pokeapp

import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import com.javaindo.pokeapp.utilities.CustomDialog
import android.net.NetworkInfo




open class PokeAppBaseActivity : AppCompatActivity(){

    private lateinit var loadingDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingDialog = CustomDialog.showLoadingDialog(this,false)

        val cm : ConnectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val nInfo : NetworkInfo = cm.activeNetworkInfo!!
        val connected : Boolean = nInfo != null && nInfo.isAvailable() && nInfo.isConnected()
//        NetworkX.isConnectedLiveData().observe(this, androidx.lifecycle.Observer {
//            when(it){
//                true ->{
//                    loadingDialog.dismiss()
//                }
//                false ->{
//                    loadingDialog.show()
//                }
//            }
//        })
    }

}