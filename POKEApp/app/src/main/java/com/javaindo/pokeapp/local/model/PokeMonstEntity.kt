package com.javaindo.pokeapp.local.model

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "pokemon_monster")
data class PokeMonstEntity(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="indexPoke")
    val indexPoke: Int?,
    @ColumnInfo(name="id")
    @NonNull
    val id: Int,
    @ColumnInfo(name="monsterName")
    val monsterName: String,
    @ColumnInfo(name="monsterUrl")
    val monsterUrl: String,
    @ColumnInfo(name = "frontPhoto")
    val frontPhoto: String,
    @ColumnInfo(name = "backPhoto")
    val backPhoto: String,
    @ColumnInfo(name = "elementPokeMonst")
    val elementPokeMonst: String
) : Parcelable
