package com.javaindo.pokeapp.local.model

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "pokemon_stat")
data class PokemonStatEntity(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="index_poke_stat")
    val index_poke_stat: Int?,
    @ColumnInfo(name="id")
    @NonNull
    val id: Int,
    @ColumnInfo(name="hp")
    val hp: Int,
    @ColumnInfo(name="atk")
    val atk: Int,
    @ColumnInfo(name = "def")
    val def: Int,
    @ColumnInfo(name = "satk")
    val satk: Int,
    @ColumnInfo(name = "sdef")
    val sdef: Int,
    @ColumnInfo(name = "spd")
    val spd: Int,
    @ColumnInfo(name = "double_dmg_from")
    val double_dmg_from: String,
    @ColumnInfo(name = "abilities")
    val abilities: String,
    @ColumnInfo(name = "sprites_normal")
    val sprites_normal: String,
    @ColumnInfo(name = "sprites_shiny")
    val sprites_shiny: String,
    @ColumnInfo(name = "species_name")
    val species_name: String,
    @ColumnInfo(name = "species_note")
    val species_note: String,
    @ColumnInfo(name = "types")
    val types: String
) : Parcelable
