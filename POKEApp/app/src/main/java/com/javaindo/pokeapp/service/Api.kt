package com.javaindo.pokeapp.service

import android.database.Observable
import com.javaindo.pokeapp.model.pokemon_monster.PokemonFormResponse
import com.javaindo.pokeapp.model.pokemon_monster.PokemonsBaseResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface Api {

        @GET("pokemon/")
        suspend fun getPokemonAllSuspend(@Query ("offset") offset:Int,
                                         @Query("limit") limit:Int)
        : PokemonsBaseResponse

        @GET("pokemon/{id}/")
        suspend fun getPokemonSuspend(@Path("id") id : Int)
        : ResponseBody

        @GET("pokemon-species/{id}/")
        suspend fun getPokemonSpeciesSuspend(@Path("id") id : Int)
        : ResponseBody

        @GET("type/{id}/")
        suspend fun getPokemonTypeSuspend(@Path("id") id : Int)
        : ResponseBody


//        @GET("pokemon-form/{id}/")
//        suspend fun getPokemonFormSuspend(@Path("id") id : Int)
//                : ResponseBody
//
//        @GET("pokemon-species/")
//        fun getPokemonSpesiesAll() : Call<ResponseBody>
//
//        @GET("pokemon/")
//        fun getPokemonAll(@Query ("offset") offset:Int, @Query("limit") limit:Int)
//        : Call<ResponseBody>
//
//        @GET("pokemon/{id}/")
//        fun getPokemon(@Path("id") id : Int) : Call<ResponseBody>
//
//        @GET("pokemon-form/{id}/")
//        fun getPokemonForm(@Path("id") id : Int) : Call<ResponseBody>
//
//
//        //=====================================================================================
//
//        @GET("pokemon/")
//        fun getPokemonAll2(@Query ("offset") offset:Int, @Query("limit") limit:Int)
//        : Observable<PokemonsBaseResponse>
//
//        @GET("pokemon-form/{id}/")
//        fun getPokemonForm2(@Path("id") id : Int)
//        : Observable<PokemonFormResponse>

}