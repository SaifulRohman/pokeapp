package com.javaindo.pokeapp.model.pokemon_monster


import com.google.gson.annotations.SerializedName

data class PokemonFormResponse(
    @SerializedName("form_name")
    val formName: String,
    @SerializedName("form_names")
    val formNames: List<Any>,
    @SerializedName("form_order")
    val formOrder: Int,
    val id: Int,
    @SerializedName("is_battle_only")
    val isBattleOnly: Boolean,
    @SerializedName("is_default")
    val isDefault: Boolean,
    @SerializedName("is_mega")
    val isMega: Boolean,
    val name: String,
    val names: List<Any>,
    val order: Int,
    val pokemon: results,
    val spritesResponse: SpritesResponse,
    val typeResponses: List<TypeResponse>,
    @SerializedName("version_group")
    val versionGroupResponse: VersionGroupResponse
)