package com.javaindo.pokeapp.utilities

import android.app.Activity
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.javaindo.pokeapp.R
import com.javaindo.pokeapp.SplashScreenActivity

object CustomDialog {

    fun showLoadingDialog( activity: Activity, cancelable: Boolean = false ): AlertDialog {
        val viewGroup: ViewGroup = activity.findViewById(android.R.id.content)
        val view = LayoutInflater.from(activity)
                .inflate(R.layout.activity_loading, viewGroup, false)
        val imgGif: ImageView = view.findViewById(R.id.imgGif)

        val builder = AlertDialog.Builder(activity)
                .setView(view)
        builder.setCancelable(cancelable)
        val alertDialog = builder.create()
        Glide.with(activity).asGif().load(R.drawable.ic_busy_loading).into(imgGif)


        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return alertDialog
    }

    fun showInternetConnectionAlert( activity: SplashScreenActivity,
                                     cancelable: Boolean = false,
                                     message : String): AlertDialog {
        val viewGroup: ViewGroup = activity.findViewById(android.R.id.content)
        val view = LayoutInflater.from(activity)
            .inflate(R.layout.dialog_internet_connection, viewGroup, false)
        val txvMessage : TextView = view.findViewById(R.id.txvMessageAlert)

        val builder = AlertDialog.Builder(activity)
            .setView(view)
        builder.setCancelable(cancelable)
        val alertDialog = builder.create()

        txvMessage.setText(message)

        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return alertDialog
    }

}