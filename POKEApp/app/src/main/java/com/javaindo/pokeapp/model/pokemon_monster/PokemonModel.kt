package com.javaindo.pokeapp.model.pokemon_monster

import androidx.room.ColumnInfo

data class PokemonModel (
    val pokeID : Int,
    val pokeName: String,
    val pokeUrl: String,
    val pokeFrontPhoto : String,
    val pokeBackPhoto : String,
    val pokeElementImage : String
)