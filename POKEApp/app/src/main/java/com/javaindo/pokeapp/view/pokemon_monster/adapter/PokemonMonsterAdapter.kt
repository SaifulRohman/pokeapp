package com.javaindo.pokeapp.view.pokemon_monster.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.javaindo.pokeapp.R
import com.javaindo.pokeapp.databinding.AdapterPokeMonsterBinding
import com.javaindo.pokeapp.model.pokemon_monster.PokemonModel
import com.javaindo.pokeapp.utilities.StringManagement

class PokemonMonsterAdapter ( diffCallback : DiffUtil.ItemCallback<PokemonModel>, val listener:(pokeModel:PokemonModel)->Unit):
        PagingDataAdapter<PokemonModel, PokemonMonsterAdapter.PokeMonsterViewHolder>(diffCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : PokeMonsterViewHolder{
        return PokeMonsterViewHolder(
            AdapterPokeMonsterBinding.inflate(LayoutInflater.from(parent.context),
                parent, false),listener
        )
    }

    override fun onBindViewHolder(holder: PokeMonsterViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    class PokeMonsterViewHolder(val binding : AdapterPokeMonsterBinding,val listener: (pokeModel :PokemonModel) -> Unit) :
        RecyclerView.ViewHolder(binding.root) {
            fun bind(data: PokemonModel){
                binding.txvMonsterName.text = data.pokeName
                if(!data.pokeFrontPhoto.isNullOrEmpty()){
                    Glide.with(binding.imgMonsterPhoto).load(data.pokeFrontPhoto).into(binding.imgMonsterPhoto)
                }
                binding.txvMonsterIndex.text = StringManagement.zeroAdd(data.pokeID.toString())

                if(!data.pokeElementImage.isNullOrEmpty()){
                    val contentArray : List<String> = data.pokeElementImage.split(";")
                    if(!contentArray.get(0).isNullOrEmpty()){
//                        val indexName: String = contentArray.get(0).substring(0,4)
                        if(contentArray.get(0).contains("bug",true)){
                            binding.imgSkill2.setImageResource(R.drawable.bug_icon)
                        } else if(contentArray.get(0).contains("dar",true)){
                            binding.imgSkill2.setImageResource(R.drawable.dark_icon)
                        } else if(contentArray.get(0).contains("dra",true)){
                            binding.imgSkill2.setImageResource(R.drawable.dragon_icon)
                        } else if(contentArray.get(0).contains("ele",true)){
                            binding.imgSkill2.setImageResource(R.drawable.electric_icon)
                        } else if(contentArray.get(0).contains("fai",true)){
                            binding.imgSkill2.setImageResource(R.drawable.fairy_icon)
                        } else if(contentArray.get(0).contains("fig",true)){
                            binding.imgSkill2.setImageResource(R.drawable.fighting_icon)
                        } else if(contentArray.get(0).contains("fir",true)){
                            binding.imgSkill2.setImageResource(R.drawable.fire_icon)
                        } else if(contentArray.get(0).contains("fly",true)){
                            binding.imgSkill2.setImageResource(R.drawable.flying_icon)
                        } else if(contentArray.get(0).contains("gho",true)){
                            binding.imgSkill2.setImageResource(R.drawable.ghost_icon)
                        } else if(contentArray.get(0).contains("gra",true)){
                            binding.imgSkill2.setImageResource(R.drawable.grass_icon)
                        } else if(contentArray.get(0).contains("gro",true)){
                            binding.imgSkill2.setImageResource(R.drawable.ground_icon)
                        } else if(contentArray.get(0).contains("ice",true)){
                            binding.imgSkill2.setImageResource(R.drawable.ice_icon)
                        } else if(contentArray.get(0).contains("nor",true)){
                            binding.imgSkill2.setImageResource(R.drawable.normal_icon)
                        } else if(contentArray.get(0).contains("poi",true)){
                            binding.imgSkill2.setImageResource(R.drawable.poison_icon)
                        } else if(contentArray.get(0).contains("psy",true)){
                            binding.imgSkill2.setImageResource(R.drawable.psychic)
                        } else if(contentArray.get(0).contains("roc",true)){
                            binding.imgSkill2.setImageResource(R.drawable.rock_icon)
                        } else if(contentArray.get(0).contains("ste",true)){
                            binding.imgSkill2.setImageResource(R.drawable.steel_icon)
                        } else if(contentArray.get(0).contains("wate",true)){
                            binding.imgSkill2.setImageResource(R.drawable.water_icon)
                        }
                    }
                    if(!contentArray.get(1).isNullOrEmpty()){
                        if(contentArray.get(1).contains("bug",true)){
                            binding.imgSkill.setImageResource(R.drawable.bug_icon)
                        } else if(contentArray.get(1).contains("dar",true)){
                            binding.imgSkill.setImageResource(R.drawable.dark_icon)
                        } else if(contentArray.get(1).contains("dra",true)){
                            binding.imgSkill.setImageResource(R.drawable.dragon_icon)
                        } else if(contentArray.get(1).contains("ele",true)){
                            binding.imgSkill.setImageResource(R.drawable.electric_icon)
                        } else if(contentArray.get(1).contains("fai",true)){
                            binding.imgSkill.setImageResource(R.drawable.fairy_icon)
                        } else if(contentArray.get(1).contains("fig",true)){
                            binding.imgSkill.setImageResource(R.drawable.fighting_icon)
                        } else if(contentArray.get(1).contains("fir",true)){
                            binding.imgSkill.setImageResource(R.drawable.fire_icon)
                        } else if(contentArray.get(1).contains("fly",true)){
                            binding.imgSkill.setImageResource(R.drawable.flying_icon)
                        } else if(contentArray.get(1).contains("gho",true)){
                            binding.imgSkill.setImageResource(R.drawable.ghost_icon)
                        } else if(contentArray.get(1).contains("gra",true)){
                            binding.imgSkill.setImageResource(R.drawable.grass_icon)
                        } else if(contentArray.get(1).contains("gro",true)){
                            binding.imgSkill.setImageResource(R.drawable.ground_icon)
                        } else if(contentArray.get(1).contains("ice",true)){
                            binding.imgSkill.setImageResource(R.drawable.ice_icon)
                        } else if(contentArray.get(1).contains("nor",true)){
                            binding.imgSkill.setImageResource(R.drawable.normal_icon)
                        } else if(contentArray.get(1).contains("poi",true)){
                            binding.imgSkill.setImageResource(R.drawable.poison_icon)
                        } else if(contentArray.get(1).contains("psy",true)){
                            binding.imgSkill.setImageResource(R.drawable.psychic)
                        } else if(contentArray.get(1).contains("roc",true)){
                            binding.imgSkill.setImageResource(R.drawable.rock_icon)
                        } else if(contentArray.get(1).contains("ste",true)){
                            binding.imgSkill.setImageResource(R.drawable.steel_icon)
                        } else if(contentArray.get(1).contains("wate",true)){
                            binding.imgSkill.setImageResource(R.drawable.water_icon)
                        }
                    }

                }

                binding.lnrBoard.setOnClickListener {
                    listener.invoke(data)
                }
            }
    }

    object PokeMonsterComparator : DiffUtil.ItemCallback<PokemonModel>() {
        override fun areItemsTheSame(oldItem: PokemonModel, newItem: PokemonModel) =
            oldItem.pokeID == newItem.pokeID

        override fun areContentsTheSame(oldItem: PokemonModel, newItem: PokemonModel) =
            oldItem == newItem
    }

    interface clickCallBack{

    }
}