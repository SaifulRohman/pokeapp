package com.javaindo.pokeapp.view.pokemon_monster.paging

import android.content.Context
import android.util.Log
import androidx.paging.PagingSource
import com.javaindo.pokeapp.repositories.PokemonMonsterRepository
import androidx.paging.PagingState
import com.javaindo.pokeapp.local.PokemonDatabase
import com.javaindo.pokeapp.local.model.PokeMonstEntity
import com.javaindo.pokeapp.model.pokemon_monster.PokemonModel
import com.javaindo.pokeapp.model.pokemon_monster.PokemonsBaseResponse
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.*
import java.io.IOException
import java.lang.Exception

private const val TMDB_STARTING_PAGE_INDEX = 20
private const val TMDB_STARTING_PRAPAGE_INDEX = 0
private var pagePraNumber = 0

class PokeMonsterPagingSource (val context: Context, val pokeMonsterRepo : PokemonMonsterRepository) :
    PagingSource<Int, PokemonModel>(){

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, PokemonModel> {

        return try{
            val pageNumber = params.key ?: TMDB_STARTING_PAGE_INDEX
            var pagePraNumber = pageNumber - TMDB_STARTING_PAGE_INDEX
            Log.d("pageNuber","offset${pagePraNumber}  limit ${pageNumber}")

            pokeMonsterRepo.processData(pagePraNumber,pageNumber)

            var pokemonList : ArrayList<PokemonModel> = ArrayList<PokemonModel>()
            val db = PokemonDatabase(context)
            val dataResult = db.PokeMonstDao().getAllPokeMonstSuspend(pagePraNumber,pageNumber)

            dataResult.forEach {
                val pokemon = PokemonModel(
                    pokeID = it.id,
                    pokeName = it.monsterName,
                    pokeUrl = it.monsterUrl,
                    pokeFrontPhoto = it.frontPhoto,
                    pokeBackPhoto = it.backPhoto,
                    pokeElementImage = it.elementPokeMonst
                )
                pokemonList.add(pokemon)
            }

//            Log.d("aaa","pageeeeeeeeeee ${pokemonList.size}")
            LoadResult.Page(
                data = pokemonList,
                prevKey = null,
                nextKey = if(pokemonList.isNullOrEmpty()) null else pageNumber+20
            )
        } catch (e : Exception){
            return LoadResult.Error(e)
        } catch (ie : IOException){
            return LoadResult.Error(ie)
        }
//        catch (he : HttpException){
//            return LoadResult.Error(he)
//        }
    }

    override fun getRefreshKey(state: PagingState<Int, PokemonModel>): Int? {
//        return super.getRefreshKey(state)
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

}