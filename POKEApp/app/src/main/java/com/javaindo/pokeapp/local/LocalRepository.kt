package com.javaindo.pokeapp.local

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.javaindo.pokeapp.local.model.PokeMonstEntity
import kotlinx.coroutines.flow.Flow

interface LocalRepository {
    fun getPagingPokeMonstLiveData(): LiveData<PagingData<PokeMonstEntity>>
    fun getPagingPokeMonstFlow(): Flow<PagingData<PokeMonstEntity>>
}