package com.javaindo.pokeapp.viewmodel

import androidx.lifecycle.ViewModel
import com.javaindo.pokeapp.repositories.PokemonDetailRepository

class PokemonDetailViewModel constructor(val pokeDetailRepo : PokemonDetailRepository): ViewModel(){

    private var idPoke : Int = 0

    suspend fun processData(){
        pokeDetailRepo.processDataPokemon(idPoke)
    }

    fun setId(id : Int){
        this.idPoke = id
    }

}