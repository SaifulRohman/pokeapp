package com.javaindo.pokeapp.repositories

import android.content.Context
import com.javaindo.pokeapp.service.RetrofitClient
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.lang.Exception
import android.util.Log
import android.widget.Toast
import com.javaindo.pokeapp.R
import com.javaindo.pokeapp.local.PokemonDatabase
import com.javaindo.pokeapp.local.model.PokeMonstEntity
import com.javaindo.pokeapp.model.pokemon_monster.PokemonParm
import com.javaindo.pokeapp.utilities.StringManagement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.collections.ArrayList


class PokemonMonsterRepository(val context2: Context) {

    suspend fun processData(offset: Int, limit: Int) {
        val pokemons = withContext(Dispatchers.IO){
            RetrofitClient.instance.getPokemonAllSuspend(offset,limit)
        }

        pokemons.results.forEach {
//            val urlarray = it.url.split("/")
//            val idString = urlarray.get((urlarray.size-2))
//            val id = if(idString != null && idString.length>0) idString.toInt() else 0

            val id = StringManagement.getIdFromUrl(it.url)
            processPokemon(id, it.name, it.url)
        }
    }

    suspend fun processPokemon(id : Int, name : String, url : String){
        val formPokemon = withContext(Dispatchers.IO){
            RetrofitClient.instance.getPokemonSuspend(id)
        }

        formPokemon.string().let {
            val jsonObject = JSONObject(it)
            val spriteObject = jsonObject.optJSONObject("sprites")

            val frontImage = spriteObject.optString("front_default")
            val backImage = spriteObject.optString("back_default")
            val typesArray = jsonObject.optJSONArray("types")

            var typesContent = ""
            for(i in 0..(typesArray.length()-1)){
                val toObject = typesArray.optJSONObject(i)
                val typeObject = toObject.optJSONObject("type")
                val nameType = typeObject.optString("name")

                typesContent = "${typesContent}${nameType};"
            }

            val pokemonEntity = PokeMonstEntity(
                null,
                id,
                name,
                url,
                frontImage,
                backImage,
                typesContent
            )

            GlobalScope.launch {
                val db = PokemonDatabase(context2)
                db.PokeMonstDao().insertPokeMonst(pokemonEntity)
            }
        }
    }

}