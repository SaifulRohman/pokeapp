package com.javaindo.pokeapp

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.javaindo.pokeapp.utilities.CustomDialog
import com.javaindo.pokeapp.utilities.InternetConnection
import com.javaindo.pokeapp.view.pokemon_monster.PokemonMonstersActivity

class SplashScreenActivity : PokeAppBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val handler = Handler()
        handler.postDelayed(object : Runnable{
            override fun run() {
                val internetConn = InternetConnection()

                if(internetConn.isInternetAvailable(applicationContext)){
                    val intent = Intent(this@SplashScreenActivity,PokemonMonstersActivity::class.java)
                    startActivity(intent)
                } else {
                    CustomDialog.showInternetConnectionAlert(this@SplashScreenActivity,false,getString(R.string.alert_internet_connection))
                }
            }

        },1000)
    }
}