package com.javaindo.pokeapp.utilities

object StringManagement {

    fun getIdFromUrl(value : String) : Int{
        val urlarray = value.split("/")
        val idString = urlarray.get((urlarray.size-2))
        val id = if(idString != null && idString.length>0) idString.toInt() else 0

        return id
    }

    fun pokeTypeIcon(name : String): String{
        var result = ""

        val indexName: String = name.substring(0,4)
        if(indexName.equals("bug_",true)){
            result = "bug_icon"
        } else if(indexName.equals("dark",true)){
            result = "dark_icon"
        } else if(indexName.equals("drag",true)){
            result = "dragon_icon"
        } else if(indexName.equals("elec",true)){
            result = "electric_icon"
        } else if(indexName.equals("fair",true)){
            result = "fairy_icon"
        } else if(indexName.equals("figh",true)){
            result = "fighting_icon"
        } else if(indexName.equals("fire",true)){
            result = "fire_icon"
        } else if(indexName.equals("flyi",true)){
            result = "flying_icon"
        } else if(indexName.equals("ghos",true)){
            result = "ghost_icon"
        } else if(indexName.equals("gras",true)){
            result = "grass_icon"
        } else if(indexName.equals("grou",true)){
            result = "ground_icon"
        } else if(indexName.equals("ice_",true)){
            result = "ice_icon"
        } else if(indexName.equals("norm",true)){
            result = "normal_icon"
        } else if(indexName.equals("pois",true)){
            result = "poison_icon"
        } else if(indexName.equals("psyc",true)){
            result = "psychic"
        } else if(indexName.equals("rock",true)){
            result = "rock_icon"
        } else if(indexName.equals("stee",true)){
            result = "steel_icon"
        } else if(indexName.equals("water",true)){
            result = "water_icon"
        }

        return result
    }

    fun zeroAdd(valString : String): String{
        var result = "000"

        if(valString.length == 1){
            result = "00${valString}"
        } else if(valString.length == 2){
            result = "0${valString}"
        } else if(valString.length == 3){
            result = valString
        }

        return result
    }

}