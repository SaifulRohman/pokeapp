package com.javaindo.pokeapp.view.pokemon_detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.google.android.material.tabs.TabLayoutMediator
import com.javaindo.pokeapp.PokeAppBaseActivity
import com.javaindo.pokeapp.R
import com.javaindo.pokeapp.databinding.ActivityPokemonDetailBinding
import com.javaindo.pokeapp.local.PokemonDatabase
import com.javaindo.pokeapp.repositories.PokemonDetailRepository
import com.javaindo.pokeapp.view.pokemon_detail.adapter.PokeMonstPagerAdapter
import com.javaindo.pokeapp.viewmodel.PokemonDetailViewModel
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

class PokemonDetailActivity : PokeAppBaseActivity(){

    lateinit var binding : ActivityPokemonDetailBinding
    lateinit var viewModel : PokemonDetailViewModel
    lateinit var idPokemon : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPokemonDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        idPokemon = intent.getStringExtra("idPokemon").toString()

        let {
            viewModel = ViewModelProvider(it,object : ViewModelProvider.Factory{
                override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                    if(modelClass.isAssignableFrom(PokemonDetailViewModel::class.java)){
                        return PokemonDetailViewModel(pokeDetailRepo = PokemonDetailRepository(applicationContext)) as T
                    } else throw IllegalArgumentException("Unknown ViewModel class")
                }
            }).get(PokemonDetailViewModel::class.java)

//            viewModel.setId(idPokemon.toInt())
//            lifecycleScope.launch {
//                viewModel.processData()
//            }

            val fragmentAdapter = PokeMonstPagerAdapter(this,idPokemon)
            binding.viewPagerMain.adapter = fragmentAdapter
            TabLayoutMediator(binding.tabsMain, binding.viewPagerMain){ tab,position ->
                tab.text = "Stats ${(position)}"
            }.attach()

        }
    }

}