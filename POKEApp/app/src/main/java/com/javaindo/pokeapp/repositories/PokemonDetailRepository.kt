package com.javaindo.pokeapp.repositories

import android.content.Context
import com.javaindo.pokeapp.local.PokemonDatabase
import com.javaindo.pokeapp.local.model.PokemonStatEntity
import com.javaindo.pokeapp.model.pokemon_monster.PokemonParmExtend
import com.javaindo.pokeapp.service.RetrofitClient
import com.javaindo.pokeapp.utilities.StringManagement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject

class PokemonDetailRepository (val context: Context){

    suspend fun processDataPokemon(id : Int){
        val pokemon = withContext(Dispatchers.IO){
            RetrofitClient.instance.getPokemonSuspend(id)
        }

        pokemon.string().let {
            val jsonObject = JSONObject(it)
            val abilitiesJsonArray = jsonObject.optJSONArray("abilities")
            val statJsonArray = jsonObject.optJSONArray("stats")
            val spritesJsonObject = jsonObject.optJSONObject("sprites")
            val speciesJsonObject = jsonObject.optJSONObject("species")
            val typesJsonArray = jsonObject.optJSONArray("types")
            val idPoke = jsonObject.optString("id")

            var pokemonParm = PokemonParmExtend()
            pokemonParm.id = if(!idPoke.isNullOrEmpty()) idPoke.toInt() else 0

            var abilitiesContent = ""
            for(i in 0..(abilitiesJsonArray.length()-1)){
                val toObject = abilitiesJsonArray.optJSONObject(i)
                val abilitObject = toObject.optJSONObject("ability")
                val name = abilitObject.optString("name")
                val url = abilitObject.optString("url")

                abilitiesContent = "${abilitiesContent}${name}|${url};"
            }
            pokemonParm.abilities = abilitiesContent

            for(i in 0..(statJsonArray.length()-1)){
                val toObject = statJsonArray.optJSONObject(i)
                val base_stat = toObject.optInt("base_stat")
                val stat = toObject.optJSONObject("stat")

                val url = stat.optString("url")
                val id = StringManagement.getIdFromUrl(url)
                if(id == 1){
                    pokemonParm.hp = base_stat
                } else if(id == 2){
                    pokemonParm.atk = base_stat
                } else if(id == 3){
                    pokemonParm.def = base_stat
                } else if(id == 4){
                    pokemonParm.satk = base_stat
                } else if(id == 5){
                    pokemonParm.sdef = base_stat
                } else if(id == 6){
                    pokemonParm.spd = base_stat
                }
            }

            val spritesNormal = spritesJsonObject.optString("front_default")
            val spritesShiny = spritesJsonObject.optString("front_shiny")
            pokemonParm.sprites_normal =spritesNormal
            pokemonParm.sprites_shiny =spritesShiny

            var typeContent = ""
            for(i in 0..(typesJsonArray.length()-1)){
                val toObject = typesJsonArray.optJSONObject(i)
                val typeObject = toObject.optJSONObject("type")
                val name = typeObject.optString("name")
                val url = typeObject.optString("url")

                typeContent = "${typeContent}${name}|${url};"
            }
            pokemonParm.types = typeContent

            val speciesName = speciesJsonObject.optString("name")
            val speciesUrl = speciesJsonObject.optString("url")
            pokemonParm.species_name = speciesName

            processDataSpecies(id,pokemonParm)
        }
    }

    suspend fun processDataSpecies(id:Int, pokemonParm : PokemonParmExtend){
        val pokemonSpecies = withContext(Dispatchers.IO){
            RetrofitClient.instance.getPokemonSpeciesSuspend(id)
        }

        pokemonSpecies.string().let {
            val jsonObject = JSONObject(it)

            val flavorTextEntryArray = jsonObject.optJSONArray("flavor_text_entries")
            for(i in 0..(flavorTextEntryArray.length()-1)){
                val toObject = flavorTextEntryArray.optJSONObject(i)
                val flavorText = toObject.optString("flavor_text")

                val versioObject = toObject.optJSONObject("version")
                val urlVer = versioObject.optString("url")
                val version = StringManagement.getIdFromUrl(urlVer)
                if(version == 8){
                    pokemonParm.species_note = flavorText
                    break
                }
            }

            processDataType(id,pokemonParm)
        }
    }

    suspend fun processDataType(id:Int, pokeParm:PokemonParmExtend){
        val pokemonType = withContext(Dispatchers.IO){
            RetrofitClient.instance.getPokemonTypeSuspend(id)
        }

        pokemonType.string().let {
            val jsonObject = JSONObject(it)
            val typeObject = jsonObject.optJSONObject("damage_relations")
            val doubleDamageFromArray = typeObject.optJSONArray("double_damage_from")

            var damageDoubleContent = ""
            for(i in 0..(doubleDamageFromArray.length()-1)){
                val toObjects = doubleDamageFromArray.optJSONObject(i)
                val name = toObjects.optString("name")
                val url = toObjects.optString("url")

                damageDoubleContent = "${damageDoubleContent}${name}|${url};"
            }
            pokeParm.double_dmg_from = damageDoubleContent

            val pokeStatEntity = PokemonStatEntity(
                null,
                pokeParm.id,
                pokeParm.hp,
                pokeParm.atk,
                pokeParm.def,
                pokeParm.satk,
                pokeParm.sdef,
                pokeParm.spd,
                pokeParm.double_dmg_from,
                pokeParm.abilities,
                pokeParm.sprites_normal,
                pokeParm.sprites_shiny,
                pokeParm.species_name,
                pokeParm.species_note,
                pokeParm.types
            )

            GlobalScope.launch {
                val db = PokemonDatabase(context)
                db.PokeStatsDao().insertPokeStats(pokeStatEntity)
            }
        }
    }


}