package com.javaindo.pokeapp.view.pokemon_monster

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import com.javaindo.pokeapp.PokeAppBaseActivity
import com.javaindo.pokeapp.R
import com.javaindo.pokeapp.databinding.ActivityPokemonMonsterBinding
import com.javaindo.pokeapp.model.pokemon_monster.PokemonModel
import com.javaindo.pokeapp.repositories.PokemonMonsterRepository
import com.javaindo.pokeapp.view.pokemon_detail.PokemonDetailActivity
import com.javaindo.pokeapp.view.pokemon_monster.adapter.PokemonMonsterAdapter
import com.javaindo.pokeapp.viewmodel.PokemonMonsterViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class PokemonMonstersActivity : PokeAppBaseActivity(){

    lateinit var binding : ActivityPokemonMonsterBinding
    lateinit var viewModel : PokemonMonsterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPokemonMonsterBinding.inflate(layoutInflater)
        setContentView(binding.root)


        let{
            viewModel = ViewModelProvider(it,object : ViewModelProvider.Factory{
                override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                    if(modelClass.isAssignableFrom(PokemonMonsterViewModel::class.java)){
                        return PokemonMonsterViewModel(pokeMonsterRepo = PokemonMonsterRepository(applicationContext),applicationContext) as T
                    } else throw IllegalArgumentException("Unknown ViewModel class")
                }
            }).get(PokemonMonsterViewModel::class.java)

            val pokeAdapter = PokemonMonsterAdapter(PokemonMonsterAdapter.PokeMonsterComparator){
                val intent = Intent(this@PokemonMonstersActivity,PokemonDetailActivity::class.java)
                intent.putExtra("idPokemon",it.pokeID.toString())
                startActivity(intent)
            }
            binding.rcylrPokMonsters.layoutManager = LinearLayoutManager(applicationContext)
            binding.rcylrPokMonsters.adapter = pokeAdapter
            lifecycleScope.launch {
//                viewModel.processData()

                viewModel.pokeFlow.collectLatest {
                    pokeAdapter.submitData(it)
                }
            }

        }


    }

}