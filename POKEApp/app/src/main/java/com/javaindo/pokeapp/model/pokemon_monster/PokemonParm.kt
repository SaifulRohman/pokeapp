package com.javaindo.pokeapp.model.pokemon_monster

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

class PokemonParm {

    var id : Int = 0
    var monsterName: String = ""
    var monsterUrl: String = ""
    var frontPhoto : String = ""
    var backPhoto : String = ""
    var elementPokeMonst : String = ""
    var urlFormId = 0

}