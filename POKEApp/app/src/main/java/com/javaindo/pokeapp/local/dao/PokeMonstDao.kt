package com.javaindo.pokeapp.local.dao

import androidx.paging.PagingSource
import androidx.room.*
import com.javaindo.pokeapp.local.model.PokeMonstEntity

@Dao
interface PokeMonstDao {

    @Query("SELECT * FROM pokemon_monster where indexPoke BETWEEN :offset AND :offsetMax")
    suspend fun getAllPokeMonstSuspend(offset: Int, offsetMax: Int):List<PokeMonstEntity>

    @Query("SELECT * FROM pokemon_monster where indexPoke BETWEEN :offset AND :offsetMax")
    fun getAllPokeMonst(offset: Int, offsetMax: Int):List<PokeMonstEntity>

    @Insert
    fun insertPokeMonst(vararg pokeMontsEntity: PokeMonstEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPokeMonstAll(pokeMontsEntityList : ArrayList<PokeMonstEntity>)

    @Delete
    fun deletePokeMonst(pokeMontsEntity: PokeMonstEntity)

    @Update
    fun udpatePokeMonts(vararg pokeMontsEntity: PokeMonstEntity)
}