package com.javaindo.pokeapp.model.pokemon_monster


import com.google.gson.annotations.SerializedName

data class TypeXResponse(
    val name: String,
    val url: String
)